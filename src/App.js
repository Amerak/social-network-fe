import React from 'react';
import './App.css';
import ApplicationRouter from './service/ApplicationRouter.js';
import {createTheme, ThemeProvider} from '@mui/material/styles';

const theme = createTheme({
    palette: {
        mode: "dark"
    }
});

export default function App() {
    return (
        <ThemeProvider theme={theme}>
            <ApplicationRouter/>
        </ThemeProvider>
    );
}
