import React from 'react'
import LoginForm from "../components/LoginForm.js";

export default function LoginRoute(props) {

    return (
        <div>
            <LoginForm {...props}/>
        </div>
    )
}
