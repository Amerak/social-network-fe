import React, {useEffect, useState} from 'react'
import Calls from "../service/Calls";
import PostCard from "../components/PostCard";
import Grid from "@mui/material/Grid";
import TopBar from "../components/TopBar";
import CreateOptions from "../components/CreateOptions";
import CreatePostModal from "../components/CreatePostModal";
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';
import AppBar from "@mui/material/AppBar";
import {Toolbar} from "@mui/material";

export default function HomeRoute(props) {

    const [posts, setPosts] = useState({itemList: []});
    const [loading, setLoading] = useState(true);
    const [modalOpen, setModalOpen] = useState(false);
    const [createSuccessSnack, setCreateSuccessSnack] = useState(false);
    const [deleteSuccessSnack, setDeleteSuccessSnack] = useState(false);
    const [createErrorSnack, setCreateErrorSnack] = useState(false);
    const [deleteErrorSnack, setDeleteErrorSnack] = useState(false);

    function handleCancel() {
        setModalOpen(false);
    }

    function handleConfirm(header, content) {
        createPost(header, content)
        setModalOpen(false);
    }

    function listPosts() {
        Calls.listPosts({}).then(result => {
            setPosts(result);
            setLoading(false);
        })
    }

    function createPost(header, content) {
        Calls.createPost({header, content}).then(result => {
                listPosts();
                result.errorCode ? setCreateErrorSnack(true) : setCreateSuccessSnack(true);
            }
        );
    }

    function deletePost(postId){
        Calls.deletePost({postId}).then(result => {
                listPosts();
                result.errorCode ? setDeleteErrorSnack(true) : setDeleteSuccessSnack(true);
            }
        );
    }

    function renderPosts() {
        return posts.itemList.map((post) => <Grid key={post.id} item> <PostCard post={post} handleDelete={deletePost}/> </Grid>)
    }

    function renderSnackBar(open, setOpen, severity, message) {
        return <Snackbar
            anchorOrigin={{vertical: "top", horizontal: "center"}}
            open={open}
            onClose={() => setOpen(false)}
            autoHideDuration={6000}
        >
            <MuiAlert onClose={() => setOpen(false)} severity={severity} sx={{width: '100%'}}>
                {message}
            </MuiAlert>
        </Snackbar>
    }

    useEffect(() => {
            listPosts(setPosts, setLoading);
        }, []
    )

    return (
        <div>
            <AppBar position="static" color={"secondary"}>
                <TopBar/>
            </AppBar>

            <Toolbar/>

            <Grid container spacing={3} justifyContent={"center"}>
                <Grid item xs={12} xl={10}>
                    <Grid container direction={"column"} spacing={2}>
                        {renderPosts()}
                    </Grid>
                </Grid>
            </Grid>

            <CreatePostModal
                open={modalOpen}
                handleCancel={handleCancel}
                handleConfirm={handleConfirm}
            />

            <CreateOptions openCreateModal={() => setModalOpen(true)}/>

            {renderSnackBar(createSuccessSnack, setCreateSuccessSnack, "success", "Post was created successfully.")}
            {renderSnackBar(createErrorSnack, setCreateErrorSnack, "error", "Post was not created.")}

            {renderSnackBar(deleteSuccessSnack, setDeleteSuccessSnack, "success", "Post was deleted successfully.")}
            {renderSnackBar(deleteErrorSnack, setDeleteErrorSnack, "error", "Post was not deleted.")}

            <Toolbar/>
            <Toolbar/>
        </div>
    )
}
