import React, {useEffect, useState} from "react";
import {useSearchParams} from "react-router-dom";
import Calls from "../service/Calls";
import TopBar from "../components/TopBar";
import Avatar from '@mui/material/Avatar';
import {red} from "@mui/material/colors";
import Typography from "@mui/material/Typography";
import {makeStyles} from "@mui/styles";
import Grid from "@mui/material/Grid";
import PostCard from "../components/PostCard";
import UserInfoCard from "../components/UserInfoCard";
import AppBar from "@mui/material/AppBar";
import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";

const useStyles = makeStyles((theme) => ({
    header: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    }
}));

export default function UserDetailsRoute(props) {
    const classes = useStyles();

    const [user, setUser] = useState({});
    const [posts, setPosts] = useState({itemList: []});
    const [userLoading, setUserLoading] = useState(true);
    const [searchParams, setSearchParams] = useSearchParams();
    const [deleteSuccessSnack, setDeleteSuccessSnack] = useState(false);
    const [deleteErrorSnack, setDeleteErrorSnack] = useState(false);

    function getUser() {
        Calls.getUserDetails({id: searchParams.get("userId")}).then(result => {
            setUser(result);
            setUserLoading(false);
        })
    }

    function listPosts() {
        Calls.listPosts({users: [searchParams.get("userId")]}).then(result => {
            setPosts(result);
        })
    }

    function deletePost(postId){
        Calls.deletePost({postId}).then(result => {
                listPosts();
                result.errorCode ? setDeleteErrorSnack(true) : setDeleteSuccessSnack(true);
            }
        );
    }

    function handleUpdateInfo() {
        setUserLoading(true);
        getUser();
    }

    useEffect(() => {
            getUser();
            listPosts();
        }, [searchParams]
    )

    function stringAvatar() {
        return {
            sx: {
                bgcolor: red[500],
                width: 80,
                height: 80
            },
            children: `${user.firstName.charAt(0).toUpperCase()}${user.surname.charAt(0).toUpperCase()}`,
        };
    }

    function renderHeader() {
        if (user.firstName && user.surname) {
            return (
                <div className={classes.header}>
                    <Avatar {...stringAvatar()} />
                    <Typography variant="h5" color="textSecondary">
                        {`${user.firstName} ${user.surname}`}
                    </Typography>
                </div>

            );
        }
    }

    function renderPosts() {
        return posts.itemList.map((post) => <Grid key={post.id} item> <PostCard post={post} handleDelete={deletePost}/> </Grid>)
    }

    function renderSnackBar(open, setOpen, severity, message) {
        return <Snackbar
            anchorOrigin={{vertical: "top", horizontal: "center"}}
            open={open}
            onClose={() => setOpen(false)}
            autoHideDuration={6000}
        >
            <MuiAlert onClose={() => setOpen(false)} severity={severity} sx={{width: '100%'}}>
                {message}
            </MuiAlert>
        </Snackbar>
    }

    return (
        <div>
            <AppBar position="static" color={"secondary"}>
                <TopBar selectedUser={user} loading={userLoading}/>
            </AppBar>

            {renderHeader()}

            <Grid container spacing={3} justifyContent={"center"}>
                <Grid item xs={12} xl={3}>
                    {!user.id ? null :
                        <UserInfoCard user={user} userLoading={userLoading} handleUpdate={handleUpdateInfo}/>}
                </Grid>
                <Grid item xs={12} xl={5}>
                    <Grid container direction={"column"} spacing={2}>
                        {renderPosts()}
                    </Grid>
                </Grid>
            </Grid>

            {renderSnackBar(deleteSuccessSnack, setDeleteSuccessSnack, "success", "Post was deleted successfully.")}
            {renderSnackBar(deleteErrorSnack, setDeleteErrorSnack, "error", "Post was not deleted.")}
        </div>
    )
}