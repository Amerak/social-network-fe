import React from 'react'
import RegisterForm from "../components/RegisterForm";

export default function RegisterRoute(props) {

    return (
        <div>
            <RegisterForm {...props}/>
        </div>
    )
}
