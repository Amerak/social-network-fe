import React, {useEffect, useState} from 'react'
import ChatDrawer from "../components/ChatDrawer";
import Calls from "../service/Calls";

export default function ChatRoute(props) {

    const [users, setUsers] = useState({itemList: []});

    useEffect(() => {
            listUsers();
        }, []
    )

    function listUsers() {
        Calls.listUsers({}).then(result => {
            setUsers(result);
        })
    }

    return (
        <div>
            {users.itemList.length > 0 ? <ChatDrawer users={users}/> : ""}
        </div>
    )
}
