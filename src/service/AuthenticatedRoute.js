import React, {createContext, useEffect, useRef, useState} from 'react'
import {Navigate} from 'react-router-dom'
import AuthenticationService from './AuthenticationService';
import Calls from "./Calls";
import CircularProgress from "@mui/material/CircularProgress";

export const UserContext = createContext();

export default function AuthenticatedRoute({children}) {

    const [user, setUser] = useState({});
    const [isLoading, setLoading] = useState(true);
    const ws = useRef(null);

    function getUser() {
        Calls.getCurrentUser().then(result => {
            setUser(result);
        })
    }

    useEffect(() => {
        if (user.username) {
            ws.current = new WebSocket(`${AuthenticationService.getSocketUri()}/chat`);
            ws.current.onopen = () => {
                setLoading(false);
                console.log("opened socket to chat")
            }
            ws.current.onclose = () => {
                setLoading(true);
                console.log("closed socket to chat")
            }

            ws.current.onmessage = e => {
                const message = JSON.parse(e.data);
                console.log("notification received: ", message)
            }

            const wsCurrent = ws.current;

            return () => {
                wsCurrent.close();
            };
        }
    }, [user])

    useEffect(() => {
            getUser();
        }, []
    )

    function renderAuthenticated(){
        if(!isLoading){
            return <UserContext.Provider value={user} children={children}/>
        }
        return <CircularProgress/>
    }

    return AuthenticationService.isUserLoggedIn() ? renderAuthenticated() :
        <Navigate to="/login"/>;
}

