export const USER_NAME_SESSION_ATTRIBUTE_NAME = 'authenticatedUser';
export const TOKEN = 'token';
export const SOCKET_URI = "socketUri";

class AuthenticationService {

    async setupToken(username, password) {
        sessionStorage.setItem(SOCKET_URI, `ws://${username}:${password}@localhost:8080`)
        sessionStorage.setItem(TOKEN, this.createBasicAuthToken(username, password));
    }

    getSocketUri() {
        return sessionStorage.getItem(SOCKET_URI);
    }

    createBasicAuthToken(username, password) {
        return 'Basic ' + window.btoa(username + ":" + password)
    }

    async registerSuccessfulLogin(username) {
        sessionStorage.setItem(USER_NAME_SESSION_ATTRIBUTE_NAME, username);
    }

    logout() {
        sessionStorage.removeItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
        sessionStorage.removeItem(TOKEN);
    }

    isUserLoggedIn() {
        let user = sessionStorage.getItem(USER_NAME_SESSION_ATTRIBUTE_NAME);
        return user !== null;
    }
}

export default new AuthenticationService()
