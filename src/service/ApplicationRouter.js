import React from 'react';
import {BrowserRouter, Route, Routes} from 'react-router-dom'
import LoginForm from '../routes/LoginRoute.js';
import RegisterRoute from '../routes/RegisterRoute.js';
import HomeRoute from "../routes/HomeRoute.js";
import AuthenticatedRoute from "./AuthenticatedRoute.js";
import UserDetailsRoute from "../routes/UserDetailsRoute";
import ChatRoute from "../routes/ChatRoute";

export default function ApplicationRouter() {

    return (
        <BrowserRouter>
            <Routes>
                <Route path="/login" element={<LoginForm/>}/>
                <Route path="/register" element={<RegisterRoute/>}/>

                <Route path="/home" element={
                    <AuthenticatedRoute>
                        <HomeRoute/>
                    </AuthenticatedRoute>}
                />

                <Route path="/chat" element={
                    <AuthenticatedRoute>
                        <ChatRoute/>
                    </AuthenticatedRoute>}
                />

                <Route path="/*" element={
                    <AuthenticatedRoute>
                        <HomeRoute/>
                    </AuthenticatedRoute>}
                />

                <Route path="/userDetails" element={
                    <AuthenticatedRoute>
                        <UserDetailsRoute/>
                    </AuthenticatedRoute>}
                />
            </Routes>
        </BrowserRouter>
    )
}
