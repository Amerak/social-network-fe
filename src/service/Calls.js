import {TOKEN} from "./AuthenticationService";

let Calls = {
    async call(method, url, dtoIn) {
        let body;
        if (dtoIn) {
            body = JSON.stringify(dtoIn);
        }

        let headers = {
            "Content-Type": "application/json; charset=utf-8",
            "Accept": "application/json",
        };
        if (sessionStorage.getItem(TOKEN)) {
            headers.authorization = sessionStorage.getItem(TOKEN)
        }
        let response = await fetch(url, {
            method: method,
            body: body,
            headers: headers
        });

        if (response.status === 401) {
            return {
                status: 401,
                errorCode: "notAuthenticated",
                message: "Not authenticated"
            }
        }
        return response.json();
    },

    getUri: function (useCase) {
        return (
            "http://localhost:8080/" + useCase
        );
    },

    async registerUser(dtoIn) {
        let commandUri = this.getUri("user/register");
        return Calls.call("post", commandUri, dtoIn);
    },

    async login() {
        let commandUri = this.getUri("basicauth");
        return Calls.call("get", commandUri);
    },

    async listPosts(dtoIn) {
        let commandUri = this.getUri("post/list");
        return Calls.call("post", commandUri, dtoIn);
    },

    async createPost(dtoIn) {
        let commandUri = this.getUri("post/create");
        return Calls.call("post", commandUri, dtoIn);
    },

    async listUsers(dtoIn) {
        let commandUri = this.getUri("user/getUserList");
        return Calls.call("post", commandUri, dtoIn);
    },

    async getUserDetails(dtoIn) {
        let commandUri = this.getUri("user/get");
        return Calls.call("post", commandUri, dtoIn);
    },

    async getCurrentUser(dtoIn) {
        let commandUri = this.getUri("user/getCurrentUser");
        return Calls.call("get", commandUri, dtoIn);
    },

    async editUser(dtoIn) {
        let commandUri = this.getUri("user/editUser");
        return Calls.call("post", commandUri, dtoIn);
    },

    async listMessages(dtoIn) {
        let commandUri = this.getUri("message/list");
        return Calls.call("post", commandUri, dtoIn);
    },

    async createComment(dtoIn) {
        let commandUri = this.getUri("comment/create");
        return Calls.call("post", commandUri, dtoIn);
    },

    async listComments(dtoIn) {
        let commandUri = this.getUri("comment/list");
        return Calls.call("post", commandUri, dtoIn);
    },

    async deletePost(dtoIn) {
        let commandUri = this.getUri("post/delete");
        return Calls.call("post", commandUri, dtoIn);
    },

    async deleteComment(dtoIn) {
        let commandUri = this.getUri("comment/delete");
        return Calls.call("post", commandUri, dtoIn);
    }
};

export default Calls;
