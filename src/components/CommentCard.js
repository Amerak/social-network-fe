import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import {red} from '@mui/material/colors';
import moment from "moment";
import {TextField} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import {useContext} from "react";
import {UserContext} from "../service/AuthenticatedRoute";

export default function CommentCard({comment, handleDelete}) {

    const currentUser = useContext(UserContext);

    function getDeleteAction() {
        if(comment.userId === currentUser.id){
            return <IconButton aria-label="settings" onClick={() => handleDelete(comment.id)}>
                <DeleteIcon/>
            </IconButton>
        }
    }

    return (
        <Card sx={{bgcolor: "#3a3b3c"}}>
            <CardHeader
                avatar={
                    <Avatar sx={{bgcolor: red[500]}} aria-label="recipe">
                        {comment.username.charAt(0)}
                    </Avatar>
                }
                action={getDeleteAction()}
                title={comment.username}
                subheader={moment(comment.date).calendar()}
            />
            <CardContent>
                <TextField fullWidth variant={"standard"} value={comment.content} multiline disabled InputProps={{
                    disableUnderline: true
                }}/>
            </CardContent>
        </Card>
    );
}