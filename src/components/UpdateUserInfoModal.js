import React, {useState} from "react";
import Modal from '@mui/material/Modal';
import Divider from '@mui/material/Divider';
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardContent from "@mui/material/CardContent";
import {makeStyles} from "@mui/styles";
import CardActions from '@mui/material/CardActions';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';

const useStyles = makeStyles((theme) => ({
    modal: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: "30%",
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    }
}));

export default function UpdateUserInfoModal({open, user, handleCancel, handleConfirm}) {
    const classes = useStyles();

    const [firstName, setFirstName] = useState(user.firstName);
    const [surname, setSurname] = useState(user.surname);

    function clearValues() {
        setFirstName(user.firstName);
        setSurname(user.surname);
    }

    return (
        <Modal
            open={open}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Card className={classes.modal}>
                <CardHeader
                    title={"Update User Details"}
                />

                <Divider/>

                <CardContent>
                    <TextField
                        value={firstName}
                        onInput={(event) => setFirstName(event.target.value)}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="firstName"
                        label="First Name"
                        name="firstName"
                        autoFocus
                    />

                    <TextField
                        value={surname}
                        onInput={(event) => setSurname(event.target.value)}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="surname"
                        label="Surname"
                        name="surname"
                    />
                </CardContent>

                <Divider/>

                <CardActions>
                    <div>
                        <Button size="small" onClick={() => {
                            clearValues();
                            handleCancel()
                        }}>Cancel</Button>
                        <Button size="small" onClick={() => {
                            handleConfirm(firstName, surname)
                        }}>Confirm</Button>
                    </div>

                </CardActions>
            </Card>

        </Modal>
    )
}
