import React, {useReducer} from 'react';
import {makeStyles} from '@mui/styles';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import Calls from "../service/Calls";
import AuthenticationService from "../service/AuthenticationService";
import Logo from "./Logo.js"
import {useNavigate} from "react-router-dom";

const initialState = {
    firstName: "",
    surname: "",
    username: "",
    email: "",
    password: "",
    matchingPassword: "",
    errors: {
        email: {
            value: false,
            message: "Zadaná hodnota musí být email."
        },
        password: {
            value: false,
            message: "Heslo musí být dlouhé minimálně 5 znaků."
        },
        matchingPassword: {
            value: false,
            message: "Heslo se neshoduje."
        },
        username: {
            value: false,
            message: "Zadané užvatelské jméno již existuje."
        }
    }
};

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(3)
    },
    gridForm: {
        marginBottom: theme.spacing(1)
    },
    submit: {
        margin: theme.spacing(3, 0, 2)
    },
}));

function reducer(state, {field, value}) {
    return {
        ...state,
        [field]: value
    }
}

async function registerClicked(state, navigate, setErrors) {
    let userInformation = {...state};
    delete (userInformation.errors);
    AuthenticationService.logout();
    let result = await Calls.registerUser(userInformation);
    console.log(result)
    if (!result.error) {
        await AuthenticationService.setupToken(userInformation.username, userInformation.password);
        await AuthenticationService.registerSuccessfulLogin(userInformation.username);
        navigate(`/home`)
    } else {
        if (result.errorCode === "user/register/usernameAlreadyExists") {
            let errors = {...state.errors};
            errors.username.value = true;
            setErrors(errors)
        } else if (result.errorCode === "user/register/emailAlreadyExists") {
            let errors = {...state.errors};
            errors.email.value = true;
            errors.email.message = "Zadaný email již existuje."
            setErrors(errors)
        }
    }
}

async function handleValidation(state, setErrors) {
    let errors = {...state.errors};
    let isValid = true;
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!re.test(state.email)) {
        errors.email.message = "Zadaná hodnota musí být email.";
        errors.email.value = true;
        isValid = false
    } else {
        errors.email.value = false;
    }

    if (state.password.length < 5) {
        errors.password.value = true;
        isValid = false
    } else {
        errors.password.value = false;
        if (state.password !== state.matchingPassword) {
            errors.matchingPassword.value = true;
            isValid = false
        } else {
            errors.matchingPassword.value = false;
        }
    }

    errors.username.value = false;
    setErrors(errors);
    return isValid;
}

export default function RegisterForm(props) {

    const classes = useStyles();
    const [state, dispatch] = useReducer(reducer, initialState);
    const navigate = useNavigate();

    const onChange = (event) => {
        dispatch({field: event.target.name, value: event.target.value})
    };

    const setErrors = (value) => {
        dispatch({field: "errors", value})
    };

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline/>
            <div className={classes.paper}>
                <Logo/>
                <Typography component="h1" variant="h5">
                    Sign up
                </Typography>
                <form className={classes.form}
                      onSubmit={async (event) => {
                          event.preventDefault();
                          if (await handleValidation(state, setErrors)) {
                              await registerClicked(state, navigate, setErrors);
                          }
                      }}>
                    <Grid container spacing={2} className={classes.gridForm}>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                autoComplete="fname"
                                name="firstName"
                                variant="outlined"
                                required
                                fullWidth
                                id="firstName"
                                label="First Name"
                                autoFocus
                                onChange={onChange}
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                id="surname"
                                label="Surname"
                                name="surname"
                                autoComplete="lname"
                                onChange={onChange}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                error={state.errors.username.value}
                                helperText={state.errors.username.value ? state.errors.username.message : null}
                                name="username"
                                variant="outlined"
                                required
                                fullWidth
                                id="username"
                                label="Username"
                                onChange={onChange}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                error={state.errors.email.value}
                                helperText={state.errors.email.value ? state.errors.email.message : null}
                                variant="outlined"
                                required
                                fullWidth
                                id="email"
                                label="Email Address"
                                name="email"
                                autoComplete="email"
                                onChange={onChange}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                error={state.errors.password.value}
                                helperText={state.errors.password.value ? state.errors.password.message : null}
                                variant="outlined"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                autoComplete="current-password"
                                onChange={onChange}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                error={state.errors.matchingPassword.value}
                                helperText={state.errors.matchingPassword.value ? state.errors.matchingPassword.message : null}
                                name="matchingPassword"
                                variant="outlined"
                                required
                                fullWidth
                                id="matchingPassword"
                                label="Matching password"
                                type="password"
                                onChange={onChange}
                            />
                        </Grid>
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Sign Up
                    </Button>
                    <Grid container justify="flex-end">
                        <Grid item>
                            <Link href="/login" variant="body2">
                                Already have an account? Sign in
                            </Link>
                        </Grid>
                    </Grid>
                </form>
            </div>
        </Container>
    );
}
