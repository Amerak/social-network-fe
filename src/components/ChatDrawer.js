import React, {useState} from 'react'
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import TopBar from "./TopBar";
import Chat from "./Chat";
import Avatar from "@mui/material/Avatar";
import {red, lightGreen} from "@mui/material/colors";

const drawerWidth = 240;

export default function ChatDrawer({users}) {

    const [mobileOpen, setMobileOpen] = useState(false);
    const [selectedUser, setSelectedUser] = useState();

    const handleDrawerToggle = () => {
        setMobileOpen(!mobileOpen);
    };

    const drawer = (
        <div>
            <Toolbar sx={{height: 100}}/>
            <List>
                {users.itemList.map((user, index) => (
                    <ListItem button key={user.username} onClick={() => setSelectedUser(user)}>
                        <ListItemIcon>
                            <Avatar sx={{bgcolor: user.active ? lightGreen[500] : red[500]}} aria-label="recipe">
                                {`${user.firstName.charAt(0).toUpperCase()}${user.surname.charAt(0).toUpperCase()}`}
                            </Avatar>
                        </ListItemIcon>
                        <ListItemText primary={user.username}/>
                    </ListItem>
                ))}
            </List>
        </div>
    );

    return (
        <Box sx={{display: 'flex'}}>
            <CssBaseline/>
            <AppBar position="fixed" sx={{zIndex: (theme) => theme.zIndex.drawer + 1}}>
                <TopBar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        edge="start"
                        onClick={handleDrawerToggle}
                        sx={{mr: 2, display: {sm: 'none'}}}
                    >
                        <MenuIcon/>
                    </IconButton>
                </TopBar>
            </AppBar>
            <Box
                component="nav"
                sx={{width: {sm: drawerWidth}, flexShrink: {sm: 0}}}
                aria-label="mailbox folders"
            >

                {/* Levá lišta pro mobil */}
                <Drawer
                    variant="temporary"
                    open={mobileOpen}
                    onClose={handleDrawerToggle}
                    ModalProps={{
                        keepMounted: true, // Better open performance on mobile.
                    }}
                    sx={{
                        display: {xs: 'block', sm: 'none'},
                        '& .MuiDrawer-paper': {boxSizing: 'border-box', width: drawerWidth},
                    }}
                >
                    {drawer}
                </Drawer>

                {/* levá lišta pro pc */}
                <Drawer
                    variant="permanent"
                    sx={{
                        display: {xs: 'none', sm: 'block'},
                        '& .MuiDrawer-paper': {boxSizing: 'border-box', width: drawerWidth},
                    }}
                    open
                >
                    {drawer}
                </Drawer>
            </Box>
            <Box
                component="main"
                sx={{flexGrow: 1, p: 3, width: {sm: `calc(100% - ${drawerWidth}px)`}}}
            >
                <Toolbar sx={{height: 100}}/>

                {selectedUser ? <Chat user={selectedUser}/> : null}
            </Box>
        </Box>
    );
}