import React from "react";
import Box from '@mui/material/Box';
import SpeedDial from '@mui/material/SpeedDial';
import SpeedDialIcon from '@mui/material/SpeedDialIcon';
import SpeedDialAction from '@mui/material/SpeedDialAction';
import PostAddIcon from '@mui/icons-material/PostAdd';
import {makeStyles} from "@mui/styles";

const useStyles = makeStyles((theme) => ({
    fabStyle: {
        margin: 0,
        top: 'auto',
        right: 20,
        bottom: 20,
        left: 'auto',
        position: 'fixed',
    }
}));

export default function CreateOptions({openCreateModal}) {
    const classes = useStyles();

    const [open, setOpen] = React.useState(false);

    function handleOpen() {
        setOpen(true);
    }

    function handleClose() {
        setOpen(false);
    }

    return (
        <Box>
            <SpeedDial
                ariaLabel="Add"
                className={classes.fabStyle}
                icon={<SpeedDialIcon/>}
                onClose={handleClose}
                onOpen={handleOpen}
                open={open}
            >
                <SpeedDialAction
                    key={'Create post'}
                    icon={<PostAddIcon/>}
                    tooltipTitle={'Create post'}
                    tooltipOpen
                    onClick={() => {
                        handleClose();
                        openCreateModal();
                    }}
                />
            </SpeedDial>
        </Box>
    );
}