import React, {useState} from "react";
import Modal from '@mui/material/Modal';
import Divider from '@mui/material/Divider';
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardContent from "@mui/material/CardContent";
import {makeStyles} from "@mui/styles";
import CardActions from '@mui/material/CardActions';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';

const useStyles = makeStyles((theme) => ({
    modal: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: "30%",
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    }
}));

export default function CreatePostModal({open, handleCancel, handleConfirm}) {
    const classes = useStyles();

    const [header, setHeader] = useState("");
    const [content, setContent] = useState("");

    return (
        <Modal
            open={open}
            aria-labelledby="modal-modal-title"
            aria-describedby="modal-modal-description"
        >
            <Card className={classes.modal}>
                <CardHeader
                    title={"Create post"}
                />

                <Divider/>

                <CardContent>
                    <TextField
                        value={header}
                        onInput={(event) => setHeader(event.target.value)}
                        multiline
                        maxRows={2}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="header"
                        label="Header"
                        name="header"
                        autoFocus
                    />

                    <TextField
                        value={content}
                        onInput={(event) => setContent(event.target.value)}
                        multiline
                        minRows={10}
                        maxRows={10}
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="content"
                        label="Content"
                        name="content"
                    />
                </CardContent>

                <Divider/>

                <CardActions>
                    <div>
                        <Button size="small" onClick={handleCancel}>Cancel</Button>
                        <Button size="small" onClick={() => {
                            setHeader("");
                            setContent("");
                            handleConfirm(header, content)
                        }}>Confirm</Button>
                    </div>

                </CardActions>
            </Card>

        </Modal>
    )
}
