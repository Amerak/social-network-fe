import React, {useReducer} from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import {makeStyles} from '@mui/styles';
import Container from '@mui/material/Container';
import Calls from "../service/Calls";
import AuthenticationService from "../service/AuthenticationService";
import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";
import Logo from "./Logo.js"
import {useNavigate} from "react-router-dom";

const initialState = {
    username: "",
    password: "",
    invalidCredentials: false
};

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

function reducer(state, {field, value}) {
    return {
        ...state,
        [field]: value
    }
}

async function loginClicked(state, navigate, dispatch) {
    await AuthenticationService.setupToken(state.username, state.password);
    try {
        let result = await Calls.login();
        console.log(result)
        if (result.errorCode === "notAuthenticated") {
            dispatch({field: "invalidCredentials", value: true})
        } else {
            await AuthenticationService.registerSuccessfulLogin(state.username);
            navigate(`/home`)
        }
    } catch (e) {
        console.log(e)
        await AuthenticationService.logout();
    }
}

function createErrorMessage(state, dispatch) {
    return (
        <Snackbar open={state.invalidCredentials} autoHideDuration={4000}
                  onClose={async () => {
                      dispatch({field: "invalidCredentials", value: false});
                  }}>
            <MuiAlert elevation={6} variant="filled" severity={"error"}>
                Zadaná kombinace uživatelského jména a hesla není správná.
            </MuiAlert>
        </Snackbar>
    )
}

export default function LoginForm(props) {
    const classes = useStyles();

    const [state, dispatch] = useReducer(reducer, initialState);
    const navigate = useNavigate();

    const onChange = (event) => {
        dispatch({field: event.target.name, value: event.target.value})
    };

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline/>
            <div className={classes.paper}>
                <Logo/>
                <Typography component="h1" variant="h5">
                    Sign in
                </Typography>
                <form className={classes.form}
                      onSubmit={async (event) => {
                          event.preventDefault();
                          await loginClicked(state, navigate, dispatch);
                      }}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="username"
                        label="Username"
                        name="username"
                        autoComplete="username"
                        autoFocus
                        onChange={onChange}

                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Password"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                        onChange={onChange}
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Sign In
                    </Button>
                    <Grid container justify="flex-end">
                        <Grid item>
                            <Link href="/register" variant="body2">
                                {"Don't have an account? Sign Up"}
                            </Link>
                        </Grid>
                    </Grid>
                </form>
            </div>
            {state.invalidCredentials ? createErrorMessage(state, dispatch) : ""}
        </Container>
    );
}
