import logo from '../res/logo.png';
import React from "react";
import {makeStyles} from "@mui/styles";
import Link from "@mui/material/Link";

const useStyles = makeStyles((theme) => ({
    logoResize: {
        width: props => props.size ? props.size : "100%",
        marginBottom: theme.spacing(2),
    }
}));

export default function Logo(props) {
    let classes = useStyles(props);

    return (
        <Link href="/home">
            <img src={logo} className={classes.logoResize} alt="logo"/>
        </Link>
    )
}