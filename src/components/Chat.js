import React, {useEffect, useState, useRef, useContext} from 'react'
import AuthenticationService from "../service/AuthenticationService";
import Calls from "../service/Calls";
import CircularProgress from "@mui/material/CircularProgress";
import ChatBubble from "./ChatBubble";
import Grid from "@mui/material/Grid";
import {UserContext} from "../service/AuthenticatedRoute";
import {InputAdornment, Toolbar} from "@mui/material";
import OutlinedInput from '@mui/material/OutlinedInput';
import IconButton from "@mui/material/IconButton";
import SendIcon from '@mui/icons-material/Send';

export default function Chat({user}) {

    const ws = useRef(null);
    const [messages, setMessages] = useState([]);
    const [isLoading, setLoading] = useState(true);
    const [text, setText] = useState("");
    const currentUser = useContext(UserContext);

    const [messReceived, setMessReceived] = useState();

    useEffect(() => {
        ws.current = new WebSocket(`${AuthenticationService.getSocketUri()}/chat/${user.username}`);
        ws.current.onopen = () => {
            console.log("(Chat) Websocket opened " + user.username)
            if (user.username) {
                Calls.listMessages({to: user.username}).then(result => {
                    setMessages(result.itemList);
                    setLoading(false);
                })
            }
        }

        ws.current.onclose = () => {
            console.log("(Chat) Websocket closed " + user.username)
            setText("");
            setMessages([]);
        }

        ws.current.onmessage = e => {
            console.log("message received: ", JSON.parse(e.data))
            setMessReceived(JSON.parse(e.data))
        }

        const wsCurrent = ws.current;

        return () => {
            wsCurrent.close();
        };
    }, [user])

    useEffect(() => {
        if (messReceived) {
            let mess = messages;
            mess.push(messReceived)
            setMessReceived(null)
            setMessages(mess)
        }
    }, [messReceived])

    function sendMessage() {
        setText("")
        ws.current.send(JSON.stringify({content: text}))
    }

    function renderMessages() {
        return messages.map((message) =>
            <Grid key={message.id} container
                  justifyContent={currentUser.username !== message.from ? "flex-start" : "flex-end"}>
                <Grid item key={message.id}>
                    <ChatBubble message={message} showFrom={currentUser.username !== message.from}/>
                </Grid>
            </Grid>
        )
    }

    return (isLoading ? <CircularProgress/> :
            <div>
                {renderMessages()}

                <Toolbar>
                    <OutlinedInput
                        value={text}
                        onChange={(event) => {
                            setText(event.target.value)
                        }}
                        multiline
                        fullWidth
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton onClick={sendMessage}>
                                    <SendIcon/>
                                </IconButton>
                            </InputAdornment>
                        }
                        onKeyDown={(e) => {
                            if (e.key === "Enter" && e.ctrlKey) {
                                sendMessage();
                            }
                        }}
                    />
                </Toolbar>
            </div>
    )
}