import React, {useContext, useState} from 'react';
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import {red} from '@mui/material/colors';
import moment from "moment";
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import {CardActions, Collapse, InputAdornment, TextField, Toolbar} from "@mui/material";
import {styled} from '@mui/material/styles';
import CommentCard from "./CommentCard";
import OutlinedInput from "@mui/material/OutlinedInput";
import SendIcon from "@mui/icons-material/Send";
import Calls from "../service/Calls";
import Grid from "@mui/material/Grid";
import DeleteIcon from '@mui/icons-material/Delete';
import {UserContext} from "../service/AuthenticatedRoute";

const ExpandMore = styled((props) => {
    const {expand, ...other} = props;
    return <IconButton {...other} />;
})(({theme, expand}) => ({
    transform: !expand ? 'rotate(0deg)' : 'rotate(180deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
    }),
}));

export default function PostCard({post, handleDelete}) {

    const [expanded, setExpanded] = useState(false);
    const [commentText, setCommentText] = useState("");
    const [isLoading, setLoading] = useState(true);
    const [comments, setComments] = useState();
    const currentUser = useContext(UserContext);

    function listComments() {
        Calls.listComments({postId: post.id}).then(result => {
            setComments(result);
            setLoading(false);
        })
    }

    function handleExpandClick() {
        setCommentText("");
        if (!expanded) {
            listComments();
        } else {
            setLoading(true);
        }
        setExpanded(!expanded);
    }

    function createComment() {
        Calls.createComment({content: commentText, postId: post.id}).then(result => {
            setCommentText("");
            listComments();
        })
    }

    function deleteComment(commentId) {
        Calls.deleteComment({commentId}).then(result => {
                listComments();
            }
        );
    }

    function renderCommentInput() {
        return <Toolbar>
            <OutlinedInput
                value={commentText}
                onChange={(event) => {
                    setCommentText(event.target.value)
                }}
                multiline
                fullWidth
                endAdornment={
                    <InputAdornment position="end">
                        <IconButton onClick={createComment}>
                            <SendIcon/>
                        </IconButton>
                    </InputAdornment>
                }
                onKeyDown={(e) => {
                    if (e.key === "Enter" && e.ctrlKey) {
                        createComment();
                    }
                }}
            />
        </Toolbar>
    }

    function renderComments() {
        if (!isLoading) {
            return comments.itemList.map((comment) => <Grid item key={comment.id} xs={12}> <CommentCard
                comment={comment} handleDelete={deleteComment}/> </Grid>)
        }
    }

    function getDeleteAction() {
        if (post.userId === currentUser.id) {
            return <IconButton aria-label="settings" onClick={() => handleDelete(post.id)}>
                <DeleteIcon/>
            </IconButton>
        }
    }

    return (
        <Card>
            <CardHeader
                avatar={
                    <Avatar sx={{bgcolor: red[500]}} aria-label="recipe">
                        {post.username.charAt(0)}
                    </Avatar>
                }
                action={getDeleteAction()}
                title={post.username}
                subheader={moment(post.date).calendar()}
            />
            <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                    {post.header}
                </Typography>
                <TextField fullWidth variant={"standard"} value={post.content} multiline disabled InputProps={{
                    disableUnderline: true
                }}/>
            </CardContent>

            <CardActions disableSpacing>
                <ExpandMore
                    expand={expanded}
                    onClick={handleExpandClick}
                    aria-expanded={expanded}
                    aria-label="show more"
                >
                    <ExpandMoreIcon/>
                </ExpandMore>
            </CardActions>

            <Collapse in={expanded} timeout="auto" unmountOnExit>
                <CardContent>
                    <Grid container direction={"column"} spacing={2}>
                        {renderComments()}
                    </Grid>
                    <Toolbar/>
                    {renderCommentInput()}
                </CardContent>
            </Collapse>
        </Card>
    );
}