import React, {useEffect, useState} from 'react';
import {makeStyles} from '@mui/styles';
import AuthenticationService from "../service/AuthenticationService";
import {createSearchParams, useNavigate} from "react-router-dom";
import {TextField, Toolbar} from "@mui/material";
import Button from "@mui/material/Button";
import Autocomplete from '@mui/material/Autocomplete';
import Calls from "../service/Calls";
import Grid from "@mui/material/Grid";
import CircularProgress from '@mui/material/CircularProgress';
import Avatar from '@mui/material/Avatar';
import smallLogo from '../res/smallLogo.png'
import LogoutIcon from '@mui/icons-material/Logout';
import HomeIcon from '@mui/icons-material/Home';
import MessageIcon from '@mui/icons-material/Message';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        float: "right"
    },
    title: {
        flexGrow: 1,
    }
}));

function handleChange(event, value, navigate) {
    if (value) {
        navigate({
            pathname: "/userDetails",
            search: `?${createSearchParams({userId: value.id})}`
        })
    }
}

function renderInput(params) {
    return <TextField {...params} label={"Search"}/>
}

function isOptionEqualToValue(option, value) {
    return option.username === value.username;
}

function listUsers(setUsers) {
    Calls.listUsers({}).then(result => {
        setUsers(result);
    })
}

function getLabel(user) {
    return `${user.firstName} "${user.username}" ${user.surname}`
}


export default function TopBar({loading, selectedUser, children}) {
    const classes = useStyles();
    const navigate = useNavigate();

    const [users, setUsers] = useState({itemList: []});

    useEffect(() => {
            listUsers(setUsers);
        }, []
    )

    return (
        <Toolbar>
            {children}
            <Grid container spacing={3} alignItems={"center"} justifyContent={"space-between"}>
                <Grid item xs={3}> {/* sx={{display: {xs: "none"}}} */}
                    <Avatar
                        alt={"test"}
                        src={smallLogo}
                        sx={{width: 80, height: 80}}
                        variant="square"
                    />
                </Grid>
                <Grid item xs={5}>
                    {loading ?
                        <CircularProgress/>
                        :
                        <Autocomplete
                            options={users.itemList.map((user) => user)}
                            renderInput={renderInput}
                            getOptionLabel={getLabel}
                            isOptionEqualToValue={isOptionEqualToValue}
                            onChange={(event, value) => handleChange(event, value, navigate)}
                            value={selectedUser ? selectedUser : null}
                        />}
                </Grid>
                <Grid item xs={3} textAlign={"end"}>
                    <Button onClick={() => {
                        navigate(`/home`);
                    }}>
                        <HomeIcon/>
                    </Button>

                    <Button onClick={() => {
                        navigate(`/chat`);
                    }}>
                        <MessageIcon/>
                    </Button>

                    <Button onClick={() => {
                        AuthenticationService.logout();
                        navigate(`/login`);
                    }}>
                        <LogoutIcon/>
                    </Button>
                </Grid>
            </Grid>
        </Toolbar>
    );
}
