import React, {useContext, useState} from "react";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import {CardActions} from "@mui/material";
import Button from "@mui/material/Button";
import {UserContext} from "../service/AuthenticatedRoute";
import UpdateUserInfoModal from "./UpdateUserInfoModal";
import Calls from "../service/Calls";

export default function UserInfoCard({user, handleUpdate}) {

    const currentUser = useContext(UserContext);
    const [updateInfoOpen, setUpdateInfoOpen] = useState(false);

    function handleUpdateInfo(firstName, surname) {
        setUpdateInfoOpen(false);
        Calls.editUser({id: currentUser.id, firstName, surname}).then(result => {
            handleUpdate();
        })
    }

    function getContent() {
        return <CardContent>
            <Typography variant="body2" color="text.secondary">
                Username: {user.username}
            </Typography>
            <Typography variant="body2" color="text.secondary">
                First name: {user.firstName}
            </Typography>
            <Typography variant="body2" color="text.secondary">
                Surname: {user.surname}
            </Typography>
            <Typography variant="body2" color="text.secondary">
                Contact email: {user.email}
            </Typography>
        </CardContent>
    }

    function getActions() {
        if (user.id === currentUser.id) {
            return <CardActions>
                <Button onClick={() => {
                    setUpdateInfoOpen(true);
                }}>
                    Update details
                </Button>
            </CardActions>
        }
    }

    return <Card>
        <CardHeader
            title={"User information:"}
        />

        {getContent()}
        {getActions()}

        <UpdateUserInfoModal open={updateInfoOpen} user={user} handleConfirm={handleUpdateInfo}
                             handleCancel={() => setUpdateInfoOpen(false)}/>

    </Card>
}