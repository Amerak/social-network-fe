import React from 'react'
import {styled} from '@mui/material/styles';
import {FormControl, InputBase, InputLabel} from "@mui/material";

const BootstrapInput = styled(InputBase)(({theme}) => ({
    'label + &': {
        marginTop: theme.spacing(2),
    },
    '& .MuiInputBase-input': {
        borderRadius: 15,
        position: 'relative',
        backgroundColor: theme.palette.mode === 'light' ? '#fcfcfb' : '#2b2b2b',
        border: '1px solid #ced4da',
        fontSize: 16,
        width: 'auto',
        padding: '10px 12px',
        transition: theme.transitions.create([
            'border-color',
            'background-color',
            'box-shadow',
        ]),
        // Use the system font instead of the default Roboto font.
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(',')
    },
}));

export default function ChatBubble({message, showFrom}) {
    return <FormControl sx={{alignContent: "right"}} disabled variant="standard">
        {showFrom ? <InputLabel sx={{justifyContent: "end"}} shrink htmlFor="bootstrap-input">
            {message.from}
        </InputLabel> : null}
        <BootstrapInput value={message.content} multiline/>
    </FormControl>
}
